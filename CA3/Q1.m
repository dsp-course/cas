clear
clc
close all
%%
load('chirp.mat')
Y = fftshift(fft(y));
w = linspace(-pi,pi,length(y));
plot(w/pi,abs(Y))
xlabel('Normalized Frequency (\times\pi rad/sample)')
ylabel('Sound''s FFT')
xlim([min(w/pi),max(w/pi)])
noise = zeros(length(y),1);
signal = zeros(length(y),1);
noise(w>-0.46*pi & w<0.46*pi) = Y(w>-0.46*pi & w<0.46*pi);
signal(w<=-0.46*pi | w>=0.46*pi) = Y(w<=-0.46*pi | w>=0.46*pi);
SNRin = snr(signal,noise);
%% Part a : Butterworth
Td = 2;
w1 = (2/Td) * tan(0.46*pi/2);
w2 = (2/Td) * tan(0.54*pi/2);
atten1 = db2mag(2);
atten2 = db2mag(30);
syms W Wc N
abs2_Hc = 1./(1+(W/Wc).^(2.*N));
eq1 = subs(abs2_Hc,W,w1) == (1/atten1)^2;
eq2 = subs(abs2_Hc,W,w2) == (1/atten2)^2;
S = solve([eq1 eq2], [Wc N]);
Wc = double(S.Wc);
N = double(round(S.N));
[z,p,k] = butter(N,Wc,'s');
[bc,ac] = zp2tf(z,p,k);
[l_bd,l_ad] = bilinear(bc,ac,1/Td);
[bd,ad] = iirlp2hp(l_bd,l_ad,0.46,0.54);
fvtool(bd,ad);
H_z = freqz(bd,ad,round(length(y)/2));
Hz = zeros(length(y),1);
for i = 1:round(length(y)/2)
    Hz(i) = H_z(round(length(y)/2)-i+1);
end
Hz(round(length(y)/2):end) = H_z;
OUT1 = Y.*Hz;
plot(w/pi,abs(OUT1))
xlabel('Normalized Frequency (\times\pi rad/sample)')
ylabel('Filtered Sound''s FFT')
xlim([min(w/pi),max(w/pi)])
out1 = abs(ifftshift(ifft(OUT1)));
sound(out1,Fs)
HZ1 = tf(bd,ad,Td);
signal1 = zeros(length(y),1);
noise1 = zeros(length(y),1);
signal1(w<=-0.46*pi | w>=0.46*pi) = OUT1(w<=-0.46*pi | w>=0.46*pi);
noise1(w>-0.46*pi & w<0.46*pi) = OUT1(w>-0.46*pi & w<0.46*pi);
SNRout1 = snr(signal1,noise1);
%% Part b : Chebyshev type I
Td = 2;
w1 = (2/Td) * tan(0.46*pi/2);
w2 = (2/Td) * tan(0.54*pi/2);
atten1 = db2mag(2);
atten2 = db2mag(30);
e = sqrt((atten1^2)-1);
syms N2
abs2_Hc2 = 1./(1+(e.^2)*(chebyshevT(N2,w2/w1).^2));
n = 0;
flag = 0;
while(flag == 0)
    n = n + 1;
    if subs(abs2_Hc2,N2,n) < (1/atten2)^2
        flag = 1;
    end
end
N2 = n;
[bc2,ac2] = cheby1(N2,2,w1,'s');
[l_bd2,l_ad2] = bilinear(bc2,ac2,1/Td);
[bd2,ad2] = iirlp2hp(l_bd2,l_ad2,0.46,0.54);
fvtool(bd2,ad2);
H_z2 = freqz(bd2,ad2,round(length(y)/2));
Hz2 = zeros(length(y),1);
for i = 1:round(length(y)/2)
    Hz2(i) = H_z2(round(length(y)/2)-i+1);
end
Hz2(round(length(y)/2):end) = H_z2;
OUT2 = Y.*Hz2;
plot(w/pi,abs(OUT2))
xlabel('Normalized Frequency (\times\pi rad/sample)')
ylabel('Filtered Sound''s FFT')
xlim([min(w/pi),max(w/pi)])
out2 = abs(ifftshift(ifft(OUT2)));
sound(out2,Fs)
HZ2 = tf(bd2,ad2,Td);
signal2 = zeros(length(y),1);
noise2 = zeros(length(y),1);
signal2(w<=-0.46*pi | w>=0.46*pi) = OUT2(w<=-0.46*pi | w>=0.46*pi);
noise2(w>-0.46*pi & w<0.46*pi) = OUT2(w>-0.46*pi & w<0.46*pi);
SNRout2 = snr(signal2,noise2);