clear
clc
close all
%% Part a
load('chirp.mat')
load('FIR filters.mat')
Y = fftshift(fft(y));
w = linspace(-pi,pi,length(y));
noise = zeros(length(y),1);
signal = zeros(length(y),1);
noise(w>-0.46*pi & w<0.46*pi) = Y(w>-0.46*pi & w<0.46*pi);
signal(w<=-0.46*pi | w>=0.46*pi) = Y(w<=-0.46*pi | w>=0.46*pi);
SNRin = snr(signal,noise);
Td = 2;
%% Part a : 1.Bartlett
H_bartlett = freqz(Num_Bartlett,1,round(length(y)/2));
H_Bart = zeros(length(y),1);
for i = 1:round(length(y)/2)
    H_Bart(i) = H_bartlett(round(length(y)/2)-i+1);
end
H_Bart(round(length(y)/2):end) = H_bartlett;
OUT_Bart = Y.*H_Bart;
figure
plot(w/pi,abs(OUT_Bart))
xlabel('Normalized Frequency (\times\pi rad/sample)')
ylabel('Filtered Sound''s FFT using Bartlett window')
xlim([min(w/pi),max(w/pi)])
out_Bart = abs(ifftshift(ifft(OUT_Bart)));
sound(out_Bart,Fs)
HZ_Bart = tf(Num_Bartlett,1,Td);
signal_Bart = zeros(length(y),1);
noise_Bart = zeros(length(y),1);
signal_Bart(w<=-0.46*pi | w>=0.46*pi) = OUT_Bart(w<=-0.46*pi | w>=0.46*pi);
noise_Bart(w>-0.46*pi & w<0.46*pi) = OUT_Bart(w>-0.46*pi & w<0.46*pi);
SNR_Bart = snr(signal_Bart,noise_Bart);
%% Part a : 2.Blackman
H_blackman = freqz(Num_Blackman,1,round(length(y)/2));
H_Blackman = zeros(length(y),1);
for i = 1:round(length(y)/2)
    H_Blackman(i) = H_blackman(round(length(y)/2)-i+1);
end
H_Blackman(round(length(y)/2):end) = H_blackman;
OUT_Blackman = Y.*H_Blackman;
figure
plot(w/pi,abs(OUT_Blackman))
xlabel('Normalized Frequency (\times\pi rad/sample)')
ylabel('Filtered Sound''s FFT using Blackman window')
xlim([min(w/pi),max(w/pi)])
out_Blackman = abs(ifftshift(ifft(OUT_Blackman)));
sound(out_Blackman,Fs)
HZ_Blackman = tf(Num_Blackman,1,Td);
signal_Blackman = zeros(length(y),1);
noise_Blackman = zeros(length(y),1);
signal_Blackman(w<=-0.46*pi | w>=0.46*pi) = OUT_Blackman(w<=-0.46*pi | w>=0.46*pi);
noise_Blackman(w>-0.46*pi & w<0.46*pi) = OUT_Blackman(w>-0.46*pi & w<0.46*pi);
SNR_Blackman = snr(signal_Blackman,noise_Blackman);
%% Part a : 3.Rectangular
H_rec = freqz(Num_Rectangular,1,round(length(y)/2));
H_Rectangular = zeros(length(y),1);
for i = 1:round(length(y)/2)
    H_Rectangular(i) = H_rec(round(length(y)/2)-i+1);
end
H_Rectangular(round(length(y)/2):end) = H_rec;
OUT_Rectangular = Y.*H_Rectangular;
figure
plot(w/pi,abs(OUT_Rectangular))
xlabel('Normalized Frequency (\times\pi rad/sample)')
ylabel('Filtered Sound''s FFT using Rectangular window')
xlim([min(w/pi),max(w/pi)])
out_Rectangular = abs(ifftshift(ifft(OUT_Rectangular)));
sound(out_Rectangular,Fs)
HZ_Rectangular = tf(Num_Rectangular,1,Td);
signal_Rectangular = zeros(length(y),1);
noise_Rectangular = zeros(length(y),1);
signal_Rectangular(w<=-0.46*pi | w>=0.46*pi) = OUT_Rectangular(w<=-0.46*pi | w>=0.46*pi);
noise_Rectangular(w>-0.46*pi & w<0.46*pi) = OUT_Rectangular(w>-0.46*pi & w<0.46*pi);
SNR_Rectangular = snr(signal_Rectangular,noise_Rectangular);
%% Part a : 4.Hamming
H_ham = freqz(Num_Hamming,1,round(length(y)/2));
H_Hamming = zeros(length(y),1);
for i = 1:round(length(y)/2)
    H_Hamming(i) = H_ham(round(length(y)/2)-i+1);
end
H_Hamming(round(length(y)/2):end) = H_ham;
OUT_Hamming = Y.*H_Hamming;
figure
plot(w/pi,abs(OUT_Hamming))
xlabel('Normalized Frequency (\times\pi rad/sample)')
ylabel('Filtered Sound''s FFT using Hamming window')
xlim([min(w/pi),max(w/pi)])
out_Hamming = abs(ifftshift(ifft(OUT_Hamming)));
sound(out_Hamming,Fs)
HZ_Hamming = tf(Num_Hamming,1,Td);
signal_Hamming = zeros(length(y),1);
noise_Hamming = zeros(length(y),1);
signal_Hamming(w<=-0.46*pi | w>=0.46*pi) = OUT_Hamming(w<=-0.46*pi | w>=0.46*pi);
noise_Hamming(w>-0.46*pi & w<0.46*pi) = OUT_Hamming(w>-0.46*pi & w<0.46*pi);
SNR_Hamming = snr(signal_Hamming,noise_Hamming);
%% Part a : 5.Hann
H_hann = freqz(Num_Hann,1,round(length(y)/2));
H_Hann = zeros(length(y),1);
for i = 1:round(length(y)/2)
    H_Hann(i) = H_hann(round(length(y)/2)-i+1);
end
H_Hann(round(length(y)/2):end) = H_hann;
OUT_Hann = Y.*H_Hann;
figure
plot(w/pi,abs(OUT_Hann))
xlabel('Normalized Frequency (\times\pi rad/sample)')
ylabel('Filtered Sound''s FFT using Hann window')
xlim([min(w/pi),max(w/pi)])
out_Hann = abs(ifftshift(ifft(OUT_Hann)));
sound(out_Hann,Fs)
HZ_Hann = tf(Num_Hann,1,Td);
signal_Hann = zeros(length(y),1);
noise_Hann = zeros(length(y),1);
signal_Hann(w<=-0.46*pi | w>=0.46*pi) = OUT_Hann(w<=-0.46*pi | w>=0.46*pi);
noise_Hann(w>-0.46*pi & w<0.46*pi) = OUT_Hann(w>-0.46*pi & w<0.46*pi);
SNR_Hann = snr(signal_Hann,noise_Hann);
%% Part b
H_hamQ = freqz(Num_Hamm_Q,1,round(length(y)/2));
H_HammQ = zeros(length(y),1);
for i = 1:round(length(y)/2)
    H_HammQ(i) = H_hamQ(round(length(y)/2)-i+1);
end
H_HammQ(round(length(y)/2):end) = H_hamQ;
OUT_HammQ = Y.*H_HammQ;
figure
plot(w/pi,abs(OUT_HammQ),'r',w/pi,abs(OUT_Hamming),'b')
xlabel('Normalized Frequency (\times\pi rad/sample)')
ylabel('Filtered Sound''s FFT using Quantized Hamming window')
xlim([min(w/pi),max(w/pi)])
legend('Quantized', 'not Quantized')
out_HammQ = abs(ifftshift(ifft(OUT_HammQ)));
sound(out_HammQ,Fs)
HZ_HammQ = tf(Num_Hamm_Q,1,Td);
signal_HammQ = zeros(length(y),1);
noise_HammQ = zeros(length(y),1);
signal_HammQ(w<=-0.46*pi | w>=0.46*pi) = OUT_HammQ(w<=-0.46*pi | w>=0.46*pi);
noise_HammQ(w>-0.46*pi & w<0.46*pi) = OUT_HammQ(w>-0.46*pi & w<0.46*pi);
SNR_HammQ = snr(signal_HammQ,noise_HammQ);