clear
clc
close all
%% part a
N = 128;
n = 0:N-1;
wc = 0.2*pi;
x = (sin(wc*(n-0.5*N))./(pi*(n-0.5*N))).^2;
x(65) = (wc/pi).^2;
figure
stem(n,x)
xlim([min(n),max(n)])
ylabel('x[n]');
xlabel('n');
X = fftshift(fft(x));
w = linspace(-0.5,0.5,length(X));
figure
plot(w,abs(X));
xlim([-0.5,0.5])
ylabel('Amplitude of FFT(128 point DFT) of x[n]');
xlabel('Digital Frequency');
figure
plot(w,angle(X));
xlim([-0.5,0.5])
ylabel('Phase of FFT(128 point DFT) of x[n]');
xlabel('Digital Frequency');
%% part b
fs = 1000;
figure
plot(w*fs,abs(X));
xlim([-0.5*fs,0.5*fs])
ylabel('Amplitude of FFT(128 point DFT) of x[n]');
xlabel('Frequency');
%% part c
y = zeros(1,2*N);
y(1:N) = x;
Y = fftshift(fft(y));
w2 = linspace(-0.5,0.5,length(Y));
figure
plot(w2,abs(Y));
xlim([-0.5,0.5])
ylabel('Amplitude of FFT(256 point DFT) of y[n]');
xlabel('Digital Frequency');
figure
plot(w2,angle(Y));
xlim([-0.5,0.5])
ylabel('Phase of FFT(256 point DFT) of y[n]');
xlabel('Digital Frequency');
%% part d
z = zeros(1,2*N);
z(1:2:2*N) = x;
Z = fftshift(fft(z));
w3 = linspace(-0.5,0.5,length(Z));
Z(w3>-0.25 & w3<0.25) = Z(w3>-0.25 & w3<0.25).*2; % lowpass filter
Z(w3<-0.25 | w3>0.25) = complex(0);
figure
plot(w3,abs(Z));
xlim([-0.25,0.25])
ylabel('Amplitude of FFT(256 point DFT) of z[n]');
xlabel('Digital Frequency');
figure
plot(w3,angle(Z));
xlim([-0.25,0.25])
ylabel('Phase of FFT(256 point DFT) of z[n]');
xlabel('Digital Frequency');