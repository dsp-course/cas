clear
clc
close all
%%
[x,fs] = audioread('DT.wav');
N = length(x);
M = fix(logspace(0,4.6,16));
E_dft = zeros(length(M),1);
E_dct = zeros(length(M),1);
X = fft(x);
X_c = dct(x);
i = 1;
for m = M
    X1 = X;
    X1_c = X_c;
    X1(1+floor((N-m)/2):floor((N+m)/2)) = 0;
    X1_c(N-m+1:end) = 0;
    x_m_dft = ifft(X1);
    x_m_dct = idct(X1_c);
    E_dft(i) = EcalcDFT(x,x_m_dft);
    E_dct(i) = EcalcDCT(x,x_m_dct);
    i = i + 1;
end

figure
semilogx(M,E_dft,'r',M,E_dct,'--y')
legend('rE[m] of DFT','rE[m] of DCT')
ylabel('rE[m]');
xlabel('m');

function rE = EcalcDFT(x,x_m_dft)
e1 = sum(abs(x-x_m_dft).^2);
e2 = sum(abs(x).^2);
rE = e1/e2;
end

function rE = EcalcDCT(x,x_m_dct)
e1 = sum(abs(x-x_m_dct).^2);
e2 = sum(abs(x).^2);
rE = e1/e2;
end