clear
clc
close all
%% part a
R = 12;
alpha = 0.8;
y_coeffs = zeros(1,R+1);
y_coeffs(1) = 1;
y_coeffs(end) = -alpha;
x_coeffs = zeros(1,R+1);
x_coeffs(end) = 1;
[h,n] = impz(x_coeffs,y_coeffs);
figure
stem(n,h)
xlim([min(n),max(n)])
ylabel('h[n] (Impulse Response h[n])'); % impulse response
xlabel('n');
H = fftshift(fft(h)); % frequency response
w = linspace(-pi,pi,length(H));
figure
plot(w,abs(H)); % amplitude
xlim([-pi,pi])
ylabel('Amplitude of Frequency Response of h[n]');
xlabel('frequency (rad/sample)');
figure
plot(w,angle(H)); % phase
xlim([-pi,pi])
ylabel('Phase of Frequency Response of h[n]');
xlabel('frequency (rad/sample)');
figure
gd = grpdelay(x_coeffs,y_coeffs,w);
plot(w,gd); % group delay
xlim([-pi,pi])
ylabel('Group Delay of Frequency Response of h[n]');
xlabel('frequency (rad/sample)');
%% part b
[x,fs] = audioread('DT.wav');
y = filter(x_coeffs,y_coeffs,x);
filename = 'output_p2_Jafarzadeh.wav';
audiowrite(filename,y,fs);