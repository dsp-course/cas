# CAs

Here are assignments of this course.

CA1:
-     Filtering ECG signal (using signal processing toolbox of MATLAB).
-     Exercises of Z transform and difference equations.

CA2:
-     Exercises of DFT & FFT, and Upsampling.
-     Implementation of a "Multi Echo Filter" and testing it with a sound.
-     Review of Energy compression of DCT.

CA3:
-     Implementation of IIR filters such as ‫‪Butterworth‬‬ and ‫‪Chebyshev‬‬.
-     Implementation of FIR filters with different windowing methods, such as ‫‪‫‪Rectangle, ‬‫‪Bartlett,‬‬ ‫‪Blackman, etc.‬‬
