clear
clc
close all
%% part a
n = 1:1000;
v = importdata('ecgtrnd.txt',' ');
subplot(2,2,1);
plot(v(n));
ylabel('ECG (mv)');
xlabel('n');
%% part b
V = fft(v);
w = linspace(0,2*pi,length(V));
subplot(2,2,2);
plot(w,abs(V));
xlim([0,2*pi])
ylabel('FFT of ECG signal');
xlabel('\omega (rad/sample)');
b = -1;
x_coeffs = [1 b];
new_v = fftfilt(x_coeffs,v);
subplot(2,2,3);
plot(new_v(n));
ylabel('filtered ECG signal (mv)');
xlabel('n');
new_V = fft(new_v);
subplot(2,2,4);
plot(w,abs(new_V));
xlim([0,2*pi])
ylabel('FFT of filtered ECG signal');
xlabel('\omega (rad/sample)');
%% part c
q1_y_coeffs = [1 1.5425 2.2663 1.7138 0.9574 0.2766];
q1_x_coeffs = [0.0612 -0.1099 0.1743 -0.1743 0.1099 -0.0612];
q1_new_v = filter(q1_x_coeffs,q1_y_coeffs,v);
figure
plot(q1_new_v(n));
ylabel('filtered ECG signal with part(a) filter  (mv)');
xlabel('n');
q1_new_V = fft(q1_new_v);
figure
plot(w,abs(q1_new_V));
xlim([0,2*pi])
ylabel('FFT of filtered ECG signal with part(a) filter  (mv)');
xlabel('\omega (rad/sample)');
h_simple = impz([1 -1],[1],length(v));
H_simple = fft(h_simple);
figure
plot(w,abs(H1))
xlim([0,2*pi])
ylabel('FFT of simple filter of part b');
xlabel('\omega (rad/sample)');
%% part d
filtered_V = V;
filtered_V(w<pi/3 | w>5*pi/3) = filtered_V(w<pi/3 | w>5*pi/3)./2;
filtered_V(w<pi/10 | w>1.9*pi) = filtered_V(w<pi/10 | w>1.9*pi)./10;
filtered_v = real(ifft(filtered_V));
figure
subplot(2,1,1);
plot(filtered_v(n));
ylabel('filtered ECG signal (mv) - new approach');
xlabel('n');
subplot(2,1,2);
plot(w,abs(filtered_V));
xlim([0,2*pi])
ylabel('FFT of filtered ECG signal - new approach');
xlabel('\omega (rad/sample)');