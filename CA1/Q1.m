clear
clc
close all
%% part a
n = 0:100;
y_coeffs = [1 1.5425 2.2663 1.7138 0.9574 0.2766];
x_coeffs = [0.0612 -0.1099 0.1743 -0.1743 0.1099 -0.0612];
h = impz(x_coeffs,y_coeffs,n);
figure
stem(n,h)
xlim([min(n),max(n)])
ylabel('h[n] (impulse response of system)');
xlabel('n');
%% part b
[H,w] = freqz(x_coeffs,y_coeffs,1000,'whole');
figure
plot(w/pi,abs(H)); % amplitude
xlabel('Normalized Frequency (\times\pi rad/sample)')
ylabel('Amplitude')
figure
plot(w/pi,angle(H)); % phase
xlabel('Normalized Frequency (\times\pi rad/sample)')
ylabel('Phase')
figure
[gd,w2] = grpdelay(x_coeffs,y_coeffs,1000,'whole');
plot(w2/pi,gd); % group delay
xlabel('Normalized Frequency (\times\pi rad/sample)')
ylabel('Group delay')
%% part c
[z,p,k] = tf2zpk(x_coeffs,y_coeffs);
zplane(z,p);
%% part d
x = (cos(0.7*pi*n)+sin(0.383*pi*n)).*(n>=0);
y = filter(x_coeffs,y_coeffs,x);
figure
stem(n,y)
xlim([min(n),max(n)])
ylabel('y[n] (output of the input x[n])');
xlabel('n');