clear
clc
close all
%% part a
f0 = 3000;
T2 = 80e-3;
omega0 = 2*pi*f0;
omega = linspace(2*pi*2500,2*pi*3500,5000);
S = (1/T2 + 1i*omega)./((1/T2)^2 + omega0^2 - omega.^2 + 1i*(2/T2)*omega);
figure
plot(omega/(2*pi),abs(S));
xlabel('\Omega (\times 2\pi rad/sec)')
ylabel('|S(j\Omega)|')
%% part b
S_abs = abs(S);
[S_max,maxIndx] = max(S_abs);
indx = sum(S_abs(maxIndx+1:end) > 0.01*max(S_abs));
indx = indx + maxIndx + 1;
cutoff_frequency = omega(indx)/(2*pi);
max_sampling_period = 1/cutoff_frequency;
fprintf('Maximum Sampling Period = %f [microsec]\n',max_sampling_period*1e6);
fprintf('Minimum Cutoff Frequency = %f [KHz]\n',cutoff_frequency/1000);
%% part c
T = 200e-6;
n = 1:1000;
s = cos(omega0*n*T).*exp(-n*T/T2);
figure
stem(n,s)
xlim([min(n),max(n)])
ylabel('s[n]');
xlabel('n');
%% part d
SNR_Q = zeros(1,length(2:15));
k = 1;
B = 2:15;
for b = B
    delta = 1/(2^b);
    codebook = -1:delta:1-delta;
    partition = -1+0.5*delta:delta:1-1.5*delta;
    [q_ind,q_s] = quantiz(s,partition,codebook); % Quantize.
    e = s - q_s;
    SNR_Q(k) = 10*(log10(0.1/var(e)));
    k = k + 1;
end
figure
stem(B+1,SNR_Q);
xlabel('B+1 (number of bits)')
ylabel('SNR of Quantization')