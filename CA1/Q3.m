clear
clc
close all
%% part a
v = importdata('hfdata.txt',' ');
s1 = v(:,1);
s2 = v(:,2);
M = 4; 
b = (1/M)*ones(1,M);
a = 1;
x1 = filter(b,a,s1);
x2 = filter(b,a,s2);
plot(s1)
hold on
plot(x1,'r')
legend('Input Data(Column 1)','Filtered Data(Column 1)')
xlim([0,length(s1)])
ylabel('Voltage');
xlabel('n');
hold off
figure
plot(s2)
hold on
plot(x2,'r')
legend('Input Data(Column 2)','Filtered Data(Column 2)')
xlim([0,length(s2)])
ylabel('Voltage');
xlabel('n');
hold off
%% part b
e1 = s1 - x1;
e2 = s2 - x2;
SNR1 = var(x1) / var(e1);
SNR2 = var(x2) / var(e2);
fprintf('SNR for signal 1: %f\n',SNR1);
fprintf('SNR for signal 1 in dB: %f\n',10*log10(SNR1));
fprintf('SNR for signal 2: %f\n',SNR2);
fprintf('SNR for signal 2 in dB: %f\n',10*log10(SNR2));
%% part c
fprintf('-------------------------\n');
fprintf('E{e1} = %f\n',mean(e1));
r1 = xcorr(e1);
fprintf('E{e2} = %f\n',mean(e2));
r2 = xcorr(e2);
subplot(2,1,1);
plot(linspace(-1000,1000,length(r1)),r1);
ylabel('R1(\tau) autocorrelation of e1');
xlabel('\tau');
subplot(2,1,2);
plot(linspace(-1000,1000,length(r2)),r2);
ylabel('R2(\tau) autocorrelation of e2');
xlabel('\tau');